# videostationplugin

#### 最近更新说明
1.  安装失败(接口变化导致)已经修复
2.  英文介绍空格丢失,已修复

#### 介绍
群晖videostation插件 豆瓣源

#### 压缩包直接安装
[com.douban.tmdb.zip](https://gitee.com/hexdgit/videostationplugin/raw/master/com.douban.tmdb.zip)


#### 源码安装

1.  打包 7z a com.douban.tmdb.zip com.douban.tmdb
2.  上传

#### 问题

1. 换行符改为unix格式
2. 如果提示权限不足,有可能下载完还没释放文件,还在读写状态,重启浏览器或者重启电脑再试一般就好了
3. qq群：874179228



